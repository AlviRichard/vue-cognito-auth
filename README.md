# Vue Authentication with AWS Amplify

> Note: It is recomended have a user with `AdministratorAccess` permission in IAM.

## Install AWS CLI

https://docs.aws.amazon.com/es_es/cli/latest/userguide/cli-chap-install.html

```
sudo apt-get install awscli
```

## Install AMPLIFY CLI

```
npm install -g @aws-amplify/cli
```

## Setup AWS credentials

```
aws configure

AWS Access Key ID [None] : `YOUR_ACCESS_KEY_ID <enter>`
AWS Secret Access Key ID [None] : `YOUR_SECRET_ACCESS_KEY_ID <enter>`
Default region name [None] : `us-east-1 <enter>`
Default output format [None]: `json <enter>`
```

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

## Amplify init

The init command must be executed at the root directory of a project to initialize the project for the Amplify CLI to work with.
See: https://aws-amplify.github.io/docs/cli-toolchain/quickstart

During the init process, the root stack is created with three resources:

- IAM role for unauthenticated users
- IAM role for authenticated users
- S3 bucket, the deployment bucket, to support this provider’s workflow

``` bash
amplify init

? Enter a name for the project `YOUR_NAME_OF_PROJECT <enter>`
# It is recommended add name without spaces and equals the name project
? Enter a name for the environment `YOUR_NAME_OF_PROJECT <enter>`
? Choose your default editor: `Visual Studio Code <enter>`
? Choose the type of app that you're building `javascript  <enter>`
Please tell us about your project
? What javascript framework are you using `vue  <enter>`
? Source Directory Path:  `src  <enter>`
? Distribution Directory Path: `dist  <enter>`
? Build Command:  `npm run-script build  <enter>`
? Start Command: `npm run-script serve  <enter>`
? Do you want to use an AWS profile? `Yes <enter>`
? Please choose the profile you want to use `default <enter>`

```

## Configuring auth with Amazon Cognito

``` bash
amplify add auth

? Do you want to use the default authentication and security configuration? `Default configuration <enter>`
? How do you want users to be able to sign in when using your Cognito User Pool? `Email <enter>`
? What attributes are required for signing up? (Press <space> to select, <a> to toggle all, <i> to invert selection)`<enter>`

```

Once you have made your category updates, run the command amplify push to update the cloud resources.

``` bash
amplify push

Current Environment: `YOUR_NAME_OF_PROJECT`

| Category | Resource name          | Operation | Provider plugin   |
| -------- | ---------------------- | --------- | ----------------- |
| Auth     | `YOUR_NAME_OF_PROJECT` | Create    | awscloudformation |
? Are you sure you want to continue? `yes`

#Updating resources in the cloud. This may take a few minutes...

```
## Setup Cognito credentials

 In your config directory edit the file `dev.env.js` and `prod.env.js` with Cognito credentials.

``` bash
 VUE_APP_URL:`'"<APP_URL>"'`,
 VUE_APP_AWS_PROJECT_REGION: `'"<AWS_PROJECT_REGION>"'`,
 VUE_APP_AWS_IDENTITY_POOL_ID: `'"<AWS_IDENTITY_POOL_ID>"'`,
 VUE_APP_AWS_USER_POOLS_ID: `'"<AWS_USER:POOLS_ID>"'`,
 VUE_APP_AWS__USER_POOLS_WEB_CLIENT_ID: `'"<AWS_USER_POOLS_WEB_CLIENT_ID>"'`

```

## Deploy in AWS S3

```
npm run build
```

```
aws s3 sync ./dist s3://vue-cognito-auth-vueauth-20191104164150-deployment --acl public-read --delete
```

## Cloud Front Create Invalidation
```
aws cloudfront create-invalidation --distribution-id E1O7DZKZO4VED9 --paths "/*"
```


## Fixed untracked files
========================

Before you need to update gitignore file and then commit or backup all your changes.

```
git rm -rf --cached .
```

Commit and push.
