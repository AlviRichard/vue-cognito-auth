'use strict';
const merge = require('webpack-merge');
const prodEnv = require('./prod.env');
module.exports = merge(prodEnv, {
    NODE_ENV: '"development"',
    VUE_APP_CALLBACK: '"http://localhost:4200/#/callback"',
    VUE_APP_AWS_PROJECT_REGION: '"us-east-1"',
    VUE_APP_AWS_IDENTITY_POOL_ID: '"us-east-1:af742879-a79f-4448-9e29-79e6d27a5d96"',
    VUE_APP_AWS_USER_POOLS_ID: '"us-east-1_l3VtiLFtP"',
    VUE_APP_AWS__USER_POOLS_WEB_CLIENT_ID: '"302daljav0cj76uj3fv8gso9vi"'
})
