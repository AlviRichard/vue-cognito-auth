import Vue from 'vue';
import Router from 'vue-router';
import { AmplifyEventBus } from 'aws-amplify-vue';
import Register from '@/components/Register';
import Login from '@/components/Login';
import Confirm from '@/components/Confirm';
import Recover from '@/components/Recover';
import RecoverSubmit from '@/components/RecoverSubmit';

Vue.use(Router);

const router = new Router({
    routes: [{
            path: '/',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: { requiresAuth: false },
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: { requiresAuth: false },
        },
        {
            path: '/confirm',
            name: 'confirm',
            component: Confirm,
            meta: { requiresAuth: false },
        },
        {
            path: '/recover',
            name: 'recover',
            component: Recover,
            meta: { requiresAuth: false },
        },
        {
            path: '/recoverSubmit',
            name: 'recoverSubmit',
            component: RecoverSubmit,
            meta: { requiresAuth: false },
        }
    ]
});

AmplifyEventBus.$on('authState', async(state) => {
    const pushPathes = {
        signedOut: () => {
            router.push({ path: '/' })
        },
        forgotPasswordSubmit: () => {
            router.push({ path: '/forgotPasswordSubmit' })
        }
    }
    if (typeof pushPathes[state] === 'function') {
        pushPathes[state]()
    }
})
export default router
