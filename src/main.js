import Vue from "vue";
import BootstrapVue from 'bootstrap-vue';

import './vendor.js';

import App from "./App.vue";
import router from "./router";
import i18n from '@/plugins/i18n';

import Amplify, * as AmplifyModules from "aws-amplify";
import { AmplifyPlugin } from "aws-amplify-vue";
import VuePhoneNumberInput from 'vue-phone-number-input';
import 'vue-phone-number-input/dist/vue-phone-number-input.css';

let region = process.env.VUE_APP_AWS_PROJECT_REGION;
let userpoolid = process.env.VUE_APP_AWS_USER_POOLS_ID;
let identitypoolid = process.env.VUE_APP_AWS_IDENTITY_POOL_ID;
let userpoolclientid = process.env.VUE_APP_AWS__USER_POOLS_WEB_CLIENT_ID;

Amplify.configure({
    Auth: {
        region: region,
        userPoolId: userpoolid,
        identityPoolId: identitypoolid,
        userPoolWebClientId: userpoolclientid
    }
});

Vue.use(BootstrapVue);
Vue.use(AmplifyPlugin, AmplifyModules);
Vue.component('vue-phone-number-input', VuePhoneNumberInput);

// It's important that you instantiate the Vue instance after calling Vue.use!
new Vue({
    router,
    i18n,
    render: h => h(App)
}).$mount("#app");
